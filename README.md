# solve-stars

## A cross-plateform plate-solve tool which works on all types of images, including RAW from camera manufacturers.

## Installation

```
sudo apt -y install git cmake qtbase5-dev libcfitsio-dev libgsl-dev wcslib-dev libraw-dev
git clone https://gitlab.com/astrophotography/solve-stars
cd solve-stars
mkdir build
cd build
cmake -DUSE_LIBRAW -DCMAKE_INSTALL_PREFIX=/usr ..
make -j $(expr $(nproc) + 2)
sudo make install
```

It is also possible to compile the tool without RAW support by omitting the `-DUSE_LIBRAW` cmake option.

## Usage

You need to install some Astrometry.net star catalogs:
```
sudo apt install astrometry-data-tycho2
```
should suffice (300 MB). However, if you have very rich star pictures (or long focal), you are adivsed to install:
```
sudo apt install astrometry-data-2mass
```
which sums up to 34 GB.

Then use the tool:
```
solve-stars image1 image2 ...
```

There are a number of options you may use as well to speed up the indexing (esp. `--ra --dec --scale-low --scale-high`)


## Credits

Heavily inspired from:

- Robert Lancaster: https://github.com/rlancaste/stellarsolver
- DomTomCat: https://stackoverflow.com/questions/20048774/convert-libraw-format-to-cvmat

Based on:

 - Astrometry.net [Astrometry README](http://astrometry.net/doc/readme.html)
 - SExtractor [Sextractor Documentation](https://sextractor.readthedocs.io/en/latest/)
 - SEP (SExtractor) [SEP Documentation](https://sep.readthedocs.io/en/v1.0.x/api/sep.extract.html)

