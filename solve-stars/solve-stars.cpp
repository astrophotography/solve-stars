// An executable to solve-plate images (determine RA/DEC location from visible stars)
//
// Build with:
// mkdir build
// cd build
// cmake .. 
// make -j 4
//
// Get index files from <http://data.astrometry.net/>
// or install     astrometry-data-tycho2 (300 MB)
// and optionally astrometry-data-2mass (39 GB)

// Qapp required to synchronize threads
#include <QApplication>
#include <time.h>

//Includes for this project
#include "structuredefinitions.h"
#include "stellarsolver.h"
#include "ssolverutils/fileio.h"

/* libRAW support */
#define USE_LIBRAW 1

// -----------------------------------------------------------------------------

void print_usage(char *pgmname)
{ /* Show program help. pgmname = argv[0] */
  printf("Usage: %s [options] img1 img2 ...\n", pgmname);
  printf("This tool determines RA/DEC location from visible stars in images (solve-plate).\n");
  printf("Version: %s (built %s)\n", StellarSolver_VERSION, StellarSolver_BUILD_TS);
  printf("The image files can be FITS, JPG, PNG, TIFF, BMP, SVG\n");
#ifdef USE_LIBRAW
  printf("  as well as common RAW camera formats.\n");
#endif
  printf("Options:\n");
  printf("  -I<dir> | -I <dir> | -d<dir> | -d <dir>\n");
  printf("      Add 'dir' to the list of locations holding Astrometry star\n");
  printf("      catalog index files. Any defined environment variable\n");
  printf("      ASTROMETRY_INDEX_FILES path will also be added.\n"); 
// these options are not actually used on stellarsolver
//  printf("  --scale-low  | -L <scale_deg>\n");
//  printf("  --scale-high | -H <scale_deg>\n");
//  printf("      Lower and Upper bound of image scale estimate, width in degrees.\n");
//  printf("  --ra  <degrees>\n");
//  printf("  --dec <degrees>\n");
//  printf("      Only search in indexes around field center given by\n");
//  printf("      'ra' and 'dec' in degrees.\n");
  printf("  --out | -o FILE\n");
  printf("      Use FILE for the output (TOML format). You may use 'stdout'.\n");
  printf("      Default is write a TOML file per image.\n");
  printf("  --out-xyls\n");
  printf("      Export in the TOML the x,y,RA,Dec of stars identified in the image.\n");
  printf("  --out-fits\n");
  printf("      Export a B&W FITS image with partial WCS information.\n");
  printf("  --overwrite | -O\n");
  printf("      Overwrite output TOML files if they already exist.\n");
  printf("  --skip-solved | -J | -K | --continue\n");
  printf("      Skip  input files for which the 'solved' output file already exists.\n");
  printf("  --help | -h\n");
  printf("      Display this help and version.\n");
  printf("  --verbose\n");
  printf("      Display detailed processing steps.\n");
  printf("  --silent\n");
  printf("      Quiet mode.\n");
  printf("The program return-code is the number of actually processed images.\n");
  exit(0);
} // print_usage

// from stellarsolver.cpp
void addPathToListIfExists(QStringList *list, QString path)
{
    if(list && QFileInfo::exists(path)) list->append(path);
}

int main(int argc, char *argv[])
{
    int         i;
    int         imageFilesCounter =0;
    int         flagVerbosity     =1;  // 0:silent 1:normal 2:verbose
    int         flagOverwrite     =0;
    int         flagFileOpen      =0;
    int         flagSkipSolved    =0;
    int         flagWriteFITS     =0; /* write a B&W FITS file */
    int         flagWriteXYLS     =0; /* write XY RA/DEC list in the TOML result file */
    char*       outputFilename    =NULL;

//    double      setRA             =NAN;
//    double      setDEC            =NAN;
//    double      setScaleLow       =NAN;
//    double      setScaleHigh      =NAN;
    QStringList catalogDirectories;
    QStringList imageFiles;
    
    QApplication app(argc, argv); // create threads
    
    catalogDirectories = StellarSolver::getDefaultIndexFolderPaths();
    
    // Parse options -----------------------------------------------------------
    clock_t t=clock();
    for (i = 1; i < argc; i++)
    {
      if (!strncmp("-I", argv[i], 2) || !strncmp("-d", argv[i], 2)) { // add a directory to hold star catalog index files
        if (strlen(argv[i]) > 2)  // -Idir
          addPathToListIfExists(&catalogDirectories, QString(argv[i]+2));
        else if (i+1 < argc) {    // -I dir
          addPathToListIfExists(&catalogDirectories, QString(argv[++i]));
        }
      } else if ((!strcmp("--out", argv[i]) || !strncmp("-o", argv[i], 3)) 
                  && i+1 < argc) { // add an output file name
        outputFilename = argv[++i];
      } else if (!strcmp("--out-fits",   argv[i])) {
        flagWriteFITS = 1;
      } else if (!strcmp("--out-xyls",   argv[i])) {
        flagWriteXYLS = 1;
//      } else if (!strcmp("--ra", argv[i]) && i+1 < argc) { 
//        setRA         = strtod(argv[++i], NULL); // set RA  in deg
//      } else if (!strcmp("--dec", argv[i]) && i+1 < argc) { 
//        setDEC        = strtod(argv[++i], NULL); // set DEC in deg
//      } else if (!strcmp("--scale-low", argv[i])   || !strcmp("-L", argv[i])) {
//        setScaleLow   = strtod(argv[++i], NULL);
//      } else if (!strcmp("--scale-high", argv[i])  || !strcmp("-H", argv[i])) {
//        setScaleHigh  = strtod(argv[++i], NULL);
      } else if (!strcmp("--overwrite",   argv[i]) || !strcmp("-O", argv[i])) {
        flagOverwrite = 1;
      } else if (!strcmp("--skip-solved", argv[i]) || !strcmp("-K", argv[i]) 
              || !strcmp("--continue",    argv[i]) || !strcmp("-J", argv[i])) {
        flagSkipSolved= 1;
      } else if (!strcmp("--verbose",     argv[i])) {  
        flagVerbosity = 2;
      } else if (!strcmp("--silent",      argv[i])) {  
        flagVerbosity = 0;
      } else if (argv[i][0] != '-')  {
        // does not start with '-' -> add a file to process
        addPathToListIfExists(&imageFiles, QString(argv[i]));
      } else {
        printf("%s: Unknown argument %s\n", argv[0], argv[i]);
        print_usage(argv[0]);  
      }
    }
    
    if (!imageFiles.count()) {
      printf("%s: No image to process.\n", argv[0]);
      printf("    Syntax: '%s image1 image2 ...' \n", argv[0]);
      printf("    Use:    '%s --help' for help\n", argv[0]);
      exit(0);
    }
      
    // Add env  ASTROMETRY_INDEX_FILES
    if (getenv("ASTROMETRY_INDEX_FILES"))
      addPathToListIfExists(&catalogDirectories, QString(getenv("ASTROMETRY_INDEX_FILES")));
    
#if defined(__linux__)
    setlocale(LC_NUMERIC, "C");
#endif
    fileio imageLoader;
    
    if (flagVerbosity >= 2) {
      printf("%s %s (built %s)\n", argv[0], StellarSolver_VERSION, StellarSolver_BUILD_TS);
      printf("INFO: Star Catalog Index Path:\n");
      for(int i = 0; i < catalogDirectories.count(); i++) {
        const QString &currentDir = catalogDirectories[i];
        printf("- %s\n", currentDir.toStdString().c_str());
      }
    }
    
    // loop on images ----------------------------------------------------------
    for(int i = 0; i < imageFiles.count(); i++)
    {
      #define BUFFER_SIZE 65535
      const QString &currentImage = imageFiles[i];
      char  currentImageName[BUFFER_SIZE];
      char  currentImagePath[BUFFER_SIZE];
      char  outputFilenameImage[BUFFER_SIZE];
      int   count;
      
      strncpy(currentImageName, QFileInfo( currentImage ).fileName().toStdString().c_str(), BUFFER_SIZE);
      strncpy(currentImagePath, currentImage.toStdString().c_str(), BUFFER_SIZE);
      count = snprintf(outputFilenameImage, BUFFER_SIZE, "%s.toml", currentImagePath);
      if (flagVerbosity >= 1)
        printf("INFO: Loading image     %s [%i/%i]\n", currentImageName, i+1, imageFiles.count());
      if (flagSkipSolved && count && QFileInfo::exists(QString(outputFilenameImage)) && flagVerbosity >= 2) {
        printf("INFO: Already solved    %s\n", currentImageName);
        continue;
      }
      if(!imageLoader.loadImage(currentImage))
      {
          fprintf(stderr, "ERROR: Can not load image %s\n", currentImageName);
          continue;
      }
      FITSImage::Statistic stats = imageLoader.getStats();
      uint8_t *imageBuffer = imageLoader.getImageBuffer();
      
      StellarSolver stellarSolver(stats, imageBuffer);
      stellarSolver.setIndexFolderPaths(catalogDirectories);

// these options are actually not used in stellarsolver
//      if (!imageLoader.position_given && !isnan(setRA) && !isnan(setDEC)) {
//        imageLoader.position_given = 1;
//        imageLoader.ra  = setRA/15.0;   // in hours
//        imageLoader.dec = setDEC;       // in deg
//      }
//      
//      if(imageLoader.position_given)
//      {
//        if (flagVerbosity >= 1)
//          printf("INFO: Using Position    %s [RA=%f hours, DEC=%f degrees]\n", 
//            currentImageName, imageLoader.ra, imageLoader.dec);
//        stellarSolver.setSearchPositionInDegrees(imageLoader.ra, imageLoader.dec);
//      }
//      
//      if (imageLoader.scale_given && !isnan(setScaleLow) && !isnan(setScaleHigh)) {
//         imageLoader.scale_given = 1;
//         imageLoader.scale_low   = setScaleLow;
//         imageLoader.scale_high  = setScaleHigh;
//         imageLoader.scale_units = DEG_WIDTH;
//      }
//      if(imageLoader.scale_given)
//      {
//          stellarSolver.setSearchScale(imageLoader.scale_low, imageLoader.scale_high, imageLoader.scale_units);
//          if (flagVerbosity >= 1)
//            printf("INFO  Using Scale       %s [%f to %f, %s]\n", 
//              currentImageName, imageLoader.scale_low, imageLoader.scale_high, SSolver::getScaleUnitString(imageLoader.scale_units).toUtf8().data());
//      }

      if (flagVerbosity >= 2)
        printf("INFO: Starting to solve %s\n", currentImageName);
      fflush( stdout );

      if(!stellarSolver.solve())
      {
          fprintf(stderr, "ERROR: Plate-Solve failed %s\n", currentImageName);
          continue;
      }

      FITSImage::Solution solution = stellarSolver.getSolution();
      
      FILE* f = NULL; // the output file
      
      if (!outputFilename) {
        // default: write a TOML file per image based on its name
        if (count)
          f = fopen(outputFilenameImage, flagOverwrite ? "w" : "a");
        if (f) {
          flagFileOpen=1;
          if (flagVerbosity >= 2)
            printf("INFO: Writing           %s\n",     outputFilenameImage);
        }
      } else if (strlen(outputFilename)) {
        // output file name given
        if (!strcmp(outputFilename, "stdout"))
          f = stdout;
        else if (!strcmp(outputFilename, "stderr"))
          f = stderr;
        else {
          f = fopen(outputFilename, flagOverwrite && !imageFilesCounter ? "w" : "a");
          if (f) {
            flagFileOpen=1;
            if (flagVerbosity >= 2 && !imageFilesCounter)
              printf("INFO: Writing           %s\n",     outputFilename);
          }
        }
      }
      if (!f) // when all failed, output to stdout
        f = stdout;
      
      // Get the FITS header/astrometry records  
      QList<fileio::Record> m_HeaderRecords = imageLoader.getRecords(); // fileio.cpp
      //    stellarsolver/astrometry/include/astrometry/sip.h
      //    stellarsolver/astrometry/util/sip.c
      tan_t tan = stellarSolver.getWCSData().wcs.wcstan;
      // add transformation matrix 'CD' records
      QList<QString>  keys  = {"CD1_1",      "CD1_2",      "CD2_1",      "CD2_2"};
      QList<QVariant> values= {tan.cd[0][0], tan.cd[0][1], tan.cd[1][0], tan.cd[1][1]};
      for (i=0; i<keys.count(); i++) {
        fileio::Record oneRecord = {keys[i], values[i], ""};
        m_HeaderRecords.append(oneRecord);
      }
      imageLoader.setRecords(m_HeaderRecords);
      
      // OUTPUT FILE -----------------------------------------------------------
      time_t t;   // not a primitive datatype
      time(&t);
      fprintf(f, "# TOML entry for image %s\n", currentImagePath);
      fprintf(f, "# date: %s", ctime(&t)); // includes \n
      fprintf(f, "[[plate_solve]]\n");
      fprintf(f, "[solve-%s]\n",                currentImageName);
      fprintf(f, "image_path            = '%s'\n", 
          currentImagePath);
      fprintf(f, "image_size            = [ %d, %d ] # pixels\n", 
          stats.width, stats.height);
      fprintf(f, "field_center          = [ %f, %f ] # deg (RA,DEC)\n", 
          solution.ra, solution.dec);
      fprintf(f, "field_centerHMS       = [ '%s','%s' ] # H:M:S (RA,DEC)\n", 
          StellarSolver::raString( solution.ra).toUtf8().data(), 
          StellarSolver::decString(solution.dec).toUtf8().data());
      fprintf(f, "field_size            = [ %f, %f ] # arcminutes\n", 
          solution.fieldWidth, solution.fieldHeight);
      fprintf(f, "field_rotation_angle  = %f # CCW / trigonometric\n", 
          360 - solution.orientation);
      fprintf(f, "field_parity          = '%s'\n",
          FITSImage::getParityText(solution.parity).toUtf8().data());
      fprintf(f, "pixel_scale           = %f #  arcseconds per pixel\n", 
          solution.pixscale);
      // add FITS header with CD matrix
      fprintf(f, "RADECSYS              = 'FK5'\n");
      fprintf(f, "CTYPE1                = 'RA---TAN'\n");
      fprintf(f, "CTYPE1                = 'DEC--TAN'\n");
      fprintf(f, "CD1_1                 = %f\n", tan.cd[0][0]);
      fprintf(f, "CD1_2                 = %f\n", tan.cd[0][1]);
      fprintf(f, "CD2_1                 = %f\n", tan.cd[1][0]);
      fprintf(f, "CD2_2                 = %f\n", tan.cd[1][1]);
      fprintf(f, "NAXIS                 = %d\n", stats.ndim);
      fprintf(f, "NAXIS1                = %d\n", stats.width);
      fprintf(f, "NAXIS2                = %d\n", stats.height);
      fprintf(f, "NAXIS3                = %d\n", stats.channels);
      fprintf(f, "CRVAL1                = %f\n", solution.ra);
      fprintf(f, "CRVAL2                = %f\n", solution.dec);
      fprintf(f, "CRPIX1                = %d\n", stats.width/2);
      fprintf(f, "CRPIX2                = %d\n", stats.height/2);
      fprintf(f, "BITPIX                = %d\n", stats.dataType);
      fprintf(f, "CROTA1                = %f\n", 360 - solution.orientation);
      fprintf(f, "SECPIX1               = %f\n", solution.pixscale);
      fflush( f );
      if (f != stdout && flagVerbosity >= 1)
        printf("INFO: %s: RA=%s DEC=%s ROT=%f [deg]\n", 
          currentImageName, 
          StellarSolver::raString( solution.ra).toUtf8().data(), 
          StellarSolver::decString(solution.dec).toUtf8().data(),
          360-solution.orientation);
      imageFilesCounter++;
      
      // save as FITS with some WCS data (--out-fits)
      if (flagWriteFITS) {
        fileio imageSaver;
        QString savePath = QString("%1.fits").arg(currentImagePath);
        imageSaver.saveAsFITS(savePath, 
          stats, imageBuffer, stellarSolver.getSolution(), m_HeaderRecords, true);
      }

      // xyls list of stars in the picture
      if (flagWriteXYLS) {
        stellarSolver.setParameterProfile(SSolver::Parameters::ALL_STARS);

        if(!stellarSolver.extract(true))
        {
            fprintf(stderr, "ERROR: Solver Star Extraction failed %s", currentImageName);
            continue;
        }

        QList<FITSImage::Star> starList = stellarSolver.getStarList();
        fprintf(f, "stars_found           = %u\n", starList.count());
        fprintf(f, "\n");
        if (flagVerbosity >= 2)
          printf("INFO: Stars found in    %s: %u\n", currentImageName, starList.count());
          
        fprintf(f, "[[stars]]\n");
        fprintf(f, "[stars-%s]\n", currentImageName);
        for(int i=0; i < starList.count(); i++)
        {
            FITSImage::Star star = starList.at(i);
            fprintf(f, "Star #%u: (%f x, %f y), (ra: %s,dec: %s), mag: %f, peak: %f, hfr: %f \n", i, star.x, star.y, 
              StellarSolver::raString( star.ra).toUtf8().data() , 
              StellarSolver::decString(star.dec).toUtf8().data(), 
              star.mag, star.peak, star.HFR);
        }
      }
      if (flagFileOpen) fclose(f);
      
    } // loop on images
    
    if (flagVerbosity >= 2) {
      double time_taken = (double)(clock() - t)/CLOCKS_PER_SEC; // calculate the elapsed time
      printf("INFO: Processed images: %u\n", imageFilesCounter);
      printf("INFO: CPU Time elapsed: %f [s]\n", time_taken);
      if (imageFilesCounter>1) 
        printf("INFO: CPU Time elapsed per image: %f [s]\n", time_taken/imageFilesCounter );
    }
    return imageFilesCounter;
}
